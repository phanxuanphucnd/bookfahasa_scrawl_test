
# coding: utf-8

# In[1]:


import scrapy
import numpy as np


# In[6]:


class FahasaBook(scrapy.Spider):
    name = 'fahasabook'
    
    def start_requests(self):
        urls = [
            'https://www.fahasa.com/sach-trong-nuoc/van-hoc-trong-nuoc.html'
        ]
#         url = "https://www.fahasa.com/sach-trong-nuoc/van-hoc-trong-nuoc/page/"
#         for i in range(2):
#             tmp = url + str(i+1) + ".html"
#             urls.append(tmp)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
            
    def parse(self, response):
        books = {}
        books['title'] = response.xpath('//h2[@class="product-name p-name-list"]/a/text()').extract()
        books['price'] = response.xpath('//span[@class="price"]/text()').extract()
        books['discount'] = response.xpath('//span[@class="p-sale-label"]/text()').extract()
        
        for key, text in books.items():
            print("{key}: {text}".format(key=key.upper(), text=text))
        
        print('LENGTH = ', len(books['title']))
        leng = len(books['title'])
        filename = "fahasa.csv"
        fields = ['title', 'price', 'discount']
        with open(filename,'a+') as f: # handle the source file
            f.write("{}\n".format('\t'.join(str(field) for field in fields))) # write header
            for i in range(leng):
                f.write("{}\n".format('\t'.join(str(books[field][i].strip()) for field in fields)))
        
